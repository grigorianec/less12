<?php

require_once 'Application.php';
require_once 'Publications.php';

Application::init();
if (!isset($_GET['id'])){
    header('Location:index.php');
}
$id = (int) $_GET['id'];

$info = Publications::create($id);

?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="main/main.css">
</head>
<body>
<?php include_once ('header.php')?>
<section class="main">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="title text-center">
                    <?=$info->title?>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="text-center">
                    <p class="fulltext">
                        <?=$info->fullDescription?>
                    </p>
                    <a class="btn" href="index.php">
                        Главная
                    </a>
                    <?php if ($info->type == 'news'):?>
                        <a class="btn" href="onlyNews.php">
                            Назад
                        </a>
                    <?php elseif ($info->type == 'article'):?>
                        <a class="btn" href="articles.php">
                            Назад
                        </a>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once ('footer.php')?>
</body>
</html>
