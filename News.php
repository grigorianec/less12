<?php
require_once ('Publications.php');

class News extends Publications
{
    public $source = 'Источник';

    public function __construct($id, $type, $title, $shortText, $fullDescription, $source)
    {
        parent::__construct($id, $type, $title, $shortText, $fullDescription);
        $this->source = $source;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getShortText()
    {
        return $this->shortText;
    }

    public function getFullDescription()
    {
        return $this->fullDescription;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function getId()
    {
        return $this->id;
    }
}