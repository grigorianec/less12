<?php

require_once ('Publications.php');

class Article extends Publications
{
    public $author = 'Автор';

    public function __construct($id, $type, $title, $shortText, $fullDescription, $author)
    {
        parent::__construct($id, $type, $title, $shortText, $fullDescription);
        $this->author = $author;
    }

}