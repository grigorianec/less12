<?php

require_once('Application.php');
require_once ('PublicationsViewer.php');
require_once ('Html.php');
require_once ('News.php');


Application::init();

try {
        $sql = 'SELECT * from list where type like "article"';
        $query = Application::$pdo->prepare($sql);
        $query->execute();
if (!$query) {
        print_r(Application::$pdo->errorInfo());
        die;
    }

$articles = $query->fetchAll();
    } catch (Exception $e) {
        echo 'Cannot select list';
        die;
    }


?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Статьи</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="main/main.css">
</head>
<body>
<?php include_once ('header.php')?>
<section class="main">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="title text-center">
                    Статьи
                </h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="news">
                    <div class="pics">
                        <img src="img/victor.jpg" alt="Ураган">
                        <img src="img/musc.jpeg" alt="Курорт">
                    </div>
                    <div class="pubs">
                        <?php foreach($articles as $article):?>
                        <p class="fulltext">
                            <?=$article['title'] ;?><br>
                            <?=$article['shortText'] ;?><br>
                            <?=$article['source'] ;?><br>
                            <a class="details" href="details.php?id=<?=$article['id']?>">
                                Детальнее
                            </a>
                        </p>
                        <?php endforeach;?>
                    </div>
                </div>
                <div>
                    <a class="btn" href="index.php">
                        Главная
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once ('footer.php')?>
</body>
</html>