<?php
class Application
{
    public static $pdo;

    public static function init()
    {
        try {
            self::$pdo = new PDO
            (
                'mysql:host=localhost;dbname=information',
                'user_information',
                '123456'
            );
        } catch (Exception $e) {
            echo 'Cannot create connection';
            die;
        }
    }
}