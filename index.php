<?php

require_once('Application.php');
require_once ('PublicationsViewer.php');
require_once ('Html.php');
require_once ('News.php');

Application::init();

$publications = new PublicationsViewer('news');

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Главная</title>
    <link rel="shortcut icon" href="img/favicon.ico">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="main/main.css">
</head>
<body>
<?php include_once ('header.php')?>
<section class="main">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="title text-center">
                    Недавние проишествия
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="news">
                    <div class="pics">
                        <img src="img/hurricane.jpg" alt="Ураган">
                        <img src="img/resort.jpg" alt="Курорт">
                        <img src="img/victor.jpg" alt="Скрипник">
                        <img src="img/musc.jpeg" alt="Илон Маск">
                    </div>
                    <div class="pubs">
                        <?=$publications->writeNews();?>
                    </div>
                </div>
                <div class="btn-backg">
                    <a class="btn" href="articles.php">
                        Статьи
                    </a>
                    <a class="btn" href="onlyNews.php">
                        Новости
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once ('footer.php')?>
</body>
</html>