<?php
require_once('Application.php');
require_once ('News.php');
require_once ('Article.php');

class PublicationsViewer
{
    public $publications = [];

    public function __construct($publicationType)
    {
        $sql = 'select * from list';
        $query = Application::$pdo->prepare($sql);
        $query->execute();
        $news = $query->fetchAll();

        foreach ($news as $new) {
            if ($new['type'] == $publicationType) {
                $this->publications[] = new News
                (
                    $new['id'],
                    $new['type'],
                    $new['title'],
                    $new['shortText'],
                    $new['fullDescription'],
                    $new['author'],
                    $new['source']
                );

            } else {
                $this->publications[] = new Article
                (
                    $new['id'],
                    $new['type'],
                    $new['title'],
                    $new['shortText'],
                    $new['fullDescription'],
                    $new['source']
                );
            }
        }
    }

    public function writeNews()
    {
        $html = '';
        foreach ($this->publications as $publication) {
            if (array_key_exists('author', $publication)) {
                $html .= '<p class="fulltext ">';
                $html .= $publication->title . '<br>';
                $html .= $publication->shortText . '<br>';
                $html .= 'Автор: ' . $publication->author . '<br>';

            } else {
                $html .= '<p class="fulltext">';
                $html .= $publication->title . '<br>';
                $html .= $publication->shortText . '<br>';
                $html .= 'Источник: ' . $publication->source . '<br>';
            }
            $html .= Html::a(
                'Смотреть полностью',
                'details.php?id=' . $publication->id
            );
            $html .= '</p>';
        }
        return $html;
    }
}


