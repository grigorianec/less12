<?php

class Publications
{
    public $id;
    public $type;
    public $title;
    public $shortText;
    public $fullDescription;

    public function __construct($id, $type, $title, $shortText, $fullDescription)
    {
        $this->id = $id;
        $this->type = $type;
        $this->title = $title;
        $this->shortText = $shortText;
        $this->fullDescription = $fullDescription;
    }


    public static function create($id)
    {
        $sql = 'select * from list where id = :id';
        $query = Application::$pdo->prepare($sql);
        $query->bindValue(':id', (int) $id);
        $query->execute();
        $info = $query->fetchObject();

        $infoObject = new Publications(
            $info->id,
            $info->type,
            $info->title,
            $info->shortText,
            $info->fullDescription
        );
        return $infoObject;
    }
}


//var_dump(Publications::create(1));