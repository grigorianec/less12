<?php
require_once('Application.php');
require_once ('PublicationsViewer.php');
require_once ('Html.php');
require_once ('News.php');


Application::init();

try {

    $sql = 'SELECT * from list where type like "news"';
    $query = Application::$pdo->prepare($sql);
    $query->execute();

    if (!$query) {
    print_r(Application::$pdo->errorInfo());
    die;
    }
    $news = $query->fetchAll();
}catch (Exception $e) {
    echo 'Cannot select list';
    die;
}

//var_dump($news);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Новости</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="main/main.css">
</head>
<body>
<?include_once ('header.php')?>
<section class="main">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="title text-center">
                    Новости
                </h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="news">
                    <div class="pics">
                        <img src="img/hurricane.jpg" alt="Ураган">
                        <img src="img/resort.jpg" alt="Курорт">
                    </div>
                    <div class="pubs">
                        <?php foreach($news as $new):?>
                        <p class="fulltext">
                            <?=$new['title'] ;?><br>
                            <?=$new['shortText'] ;?><br>
                            <?=$new['author'] ;?><br>
                            <a class="details" href="details.php?id=<?=$new['id']?>">
                                Детальнее
                            </a>
                        </p>
                        <?php endforeach;?>
                    </div>
                </div>
                <div>
                    <a class="btn" href="index.php">
                        Главная
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<?include_once ('footer.php')?>
</body>
</html>